$(document).ready(function () {
    $("#menu_button").click(function () {
        $("#menu_mobile").toggle("slow");
    });
});

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
    }
});


var waypoint = new Waypoint({
    element: document.getElementById("the_target"),
    handler: function() {
        $('.number').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 2000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    },
    offset:'bottom-in-view'
});